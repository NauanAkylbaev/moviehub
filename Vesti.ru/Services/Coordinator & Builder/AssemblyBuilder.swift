//
//  AssemblyBuilder.swift
//  Vesti.ru
//
//  Created by a on 24.09.2020.
//  Copyright © 2020 a. All rights reserved.
//

import UIKit

protocol AssemblyBuilderProtocol: class {
    func createMainNewsModule(coordinator: NewsCoordinatorProtocol) -> UIViewController
    func createDetailNewsModule(news: RSSItem, coordinator: NewsCoordinatorProtocol) -> UIViewController
}

final class AssemblyBuilder: AssemblyBuilderProtocol {
    
    func createMainNewsModule(coordinator: NewsCoordinatorProtocol) -> UIViewController {
        let view = NewsCVC(collectionViewLayout: UICollectionViewFlowLayout())
        let networkServices = NewsServices()
        let model = NewsModel(networkServices: networkServices)
        let presenter = MainNewsPresenter(view: view, model: model, coordinator: coordinator)
        view.presenter = presenter
        return view
    }
    
    func createDetailNewsModule(news: RSSItem, coordinator: NewsCoordinatorProtocol) -> UIViewController {
        let view = DetailNewsVC()
        let networkServices = DownloadImageService()
        let model = DetailNewsModel(networkServices: networkServices)
        let presenter = DetailNewsPresenter(news: news, view: view, model: model,coordinator: coordinator)
        view.presenter = presenter
        return view
    }
    
}
