//
//  AppCoordinator.swift
//  Vesti.ru
//
//  Created by a on 24.09.2020.
//  Copyright © 2020 a. All rights reserved.
//

import UIKit

protocol СoordinatorProtocol: class {
    var navigationController: UINavigationController { get set }
    var assemblyBuilder: AssemblyBuilderProtocol? { get set }
    func start()
}
