//
//  DownloadImageService.swift
//  Vesti.ru
//
//  Created by a on 24.09.2020.
//  Copyright © 2020 a. All rights reserved.
//

import Foundation

enum URLErrors: Error {
    case wrongUrl
    case couldntMakeJson
}

protocol DownloadImageServiceProtocol: class {
    func downloadImage(urlAdress: String, completionHandler: @escaping ((Result<Data, Error>) -> Void))
}

final class DownloadImageService: DownloadImageServiceProtocol {
    
    func downloadImage(urlAdress: String, completionHandler: @escaping ((Result<Data, Error>) -> Void)) {
        
        guard let urlPath = URL(string: urlAdress) else {
            completionHandler(.failure(URLErrors.wrongUrl))
            return
        }
        
        let task = URLSession.shared.dataTask(with: urlPath) { (data, _, err) in
            if let error = err {
                completionHandler(.failure(error))
            } else if let unwrappedData = data {
                completionHandler(.success(unwrappedData))
            }
        }
        
        task.resume()
    }
    
}

