//
//  NetworkService.swift
//  Vesti.ru
//
//  Created by a on 24.09.2020.
//  Copyright © 2020 a. All rights reserved.
//

import Foundation

enum URLAdresses: String {
    case vesti = "http://www.vesti.ru/vesti.rss"
}

struct RSSItem {
    var title: String
    var description: String
    var publicDate: String
    var link: String
    var category: String
    var fullNews: String
    var imageURL: String
}

protocol NewsServicesProtocol: class {
    func downloadNews(url: String, completionHandler: @escaping ((Result<[RSSItem], Error>) -> Void))
}

//MARK: - XML parser, this parser download news from Vesti
final class NewsServices: NSObject, XMLParserDelegate, NewsServicesProtocol {
    
    private var rssItems: [RSSItem] = []
    private var currentElement = String()
    
    private var currentTitle = String()
    private var currentDescription = String()
    private var currentPublicDate = String()
    private var currentLink = String()
    private var currentCategory = String()
    private var currentfullNews = String()
    private var currentImageURL = String()
    
    private var parserCompletionHandler: ((Result<[RSSItem], Error>) -> Void)!
    
    //MARK: - Methods
    public func downloadNews(url: String, completionHandler: @escaping ((Result<[RSSItem], Error>) -> Void)) {
        parserCompletionHandler = completionHandler
        
        let config = URLSessionConfiguration.default
        config.requestCachePolicy = .reloadIgnoringLocalCacheData
        config.urlCache = nil
        let session = URLSession.init(configuration: config)
        
        let request = URLRequest(url: URL(string: url)!)
        let task = session.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
                if let error = error {
                    completionHandler(.failure(error))
                }
                return
            }
            let parser = XMLParser(data: data)
            parser.delegate = self
            parser.parse()
        }
        
        task.resume()
    }
    
    // MARK: - XML Parser Delegate
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        currentElement = elementName
        //When parser get new element, current title, description and pubDate removes
        if currentElement == "item" {
            currentTitle = String()
            currentDescription = String()
            currentPublicDate = String()
            currentLink = String()
            currentCategory = String()
            currentImageURL = String()
            currentfullNews = String()
        }
        
        if currentElement == "enclosure" {
            currentImageURL = attributeDict["url"] ?? ""
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        
        let fulltext = """
yandex:full-text
"""
        switch currentElement {
        case "title":
            currentTitle += string
        case "description":
            currentDescription += string
        case "pubDate":
            currentPublicDate += string
        case "link":
            currentLink += string
        case "category":
            currentCategory += string
        case fulltext:
            currentfullNews += string
        default:
            break
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        
        if elementName == "item" {
            let rssItem = RSSItem(title: currentTitle,
                                  description: currentDescription,
                                  publicDate: currentPublicDate,
                                  link: currentLink,
                                  category: currentCategory,
                                  fullNews: currentfullNews,
                                  imageURL: currentImageURL)
            
            self.rssItems.append(rssItem)
        }
    }
    
    func parserDidEndDocument(_ parser: XMLParser) {
        parserCompletionHandler(.success(rssItems))
        rssItems.removeAll()
    }
    
    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        parserCompletionHandler(.failure(parseError))
    }
}
