//
//  DetailNewsModel.swift
//  Vesti.ru
//
//  Created by a on 24.09.2020.
//  Copyright © 2020 a. All rights reserved.
//

import Foundation

protocol DetailNewsModelProtocol {
    init(networkServices: DownloadImageServiceProtocol)
    func downloadImage(urlAdress: String, completionHandler: @escaping ((Result<Data, Error>) -> Void))
}

final class DetailNewsModel: DetailNewsModelProtocol {
    
    var networkServices: DownloadImageServiceProtocol
    
    init(networkServices: DownloadImageServiceProtocol) {
        self.networkServices = networkServices
    }
    
    func downloadImage(urlAdress: String, completionHandler: @escaping ((Result<Data, Error>) -> Void)) {
        networkServices.downloadImage(urlAdress: urlAdress) { result in
            switch result {
            case .failure(let err):
                completionHandler(.failure(err))
            case .success(let data):
                completionHandler(.success(data))
            }
        }
    }
    
}

