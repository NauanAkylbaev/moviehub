//
//  DetailNewsView.swift
//  Vesti.ru
//
//  Created by a on 24.09.2020.
//  Copyright © 2020 a. All rights reserved.
//

import UIKit

final class DetailNewsView {
    
    private let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    
    public let mainImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.alpha = 0.0
        return imageView
    }()
    
    public let mainTitlelabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.font = UIFont(name: "Helvetica-Bold", size: 25)
        label.textColor = .textColor
        return label
    }()
    
    public let fullNewsLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.font = UIFont(name: "Helvetica", size: 16)
        label.textColor = .textColor
        return label
    }()
    
    public func setConstraints(view: UIView, isImageExist: Bool) -> UIView {
        view.addSubview(scrollView)
        scrollView.addSubview(mainTitlelabel)
        scrollView.addSubview(fullNewsLabel)
        
        if isImageExist {
            scrollView.addSubview(mainImageView)
            
            NSLayoutConstraint.activate([
                mainImageView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: +10),
                mainImageView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: +10),
                mainImageView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -10),
                mainImageView.heightAnchor.constraint(equalToConstant: view.frame.height * 0.3)
            ])
            
            mainTitlelabel.topAnchor.constraint(equalTo: mainImageView.bottomAnchor, constant: +20).isActive = true
        } else {
            mainTitlelabel.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: +10).isActive = true
        }
        
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            scrollView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            scrollView.widthAnchor.constraint(equalTo: view.widthAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        
        NSLayoutConstraint.activate([
            mainTitlelabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: +10),
            mainTitlelabel.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -10),
        ])
        
        NSLayoutConstraint.activate([
             fullNewsLabel.topAnchor.constraint(equalTo: mainTitlelabel.bottomAnchor, constant: +15),
             fullNewsLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: +10),
             fullNewsLabel.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -10),
             fullNewsLabel.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: -20)
         ])
        
        return view
    }
    
    
}

