//
//  DetailNewsVC.swift
//  Vesti.ru
//
//  Created by a on 24.09.2020.
//  Copyright © 2020 a. All rights reserved.
//

import UIKit

final class DetailNewsVC: UIViewController {

    public var presenter: DetailNewsPresenterProtocol!
    private var detailNewsView = DetailNewsView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeView()
        setDataToView()
    }
    
    private func customizeView() {
        navigationItem.largeTitleDisplayMode = .never
        navigationController!.navigationBar.topItem!.title = "Назад"
        title = presenter.news.category.capitalized
        view.backgroundColor = UIColor.backGroundColor
        
        let url = presenter.news.imageURL.filter({$0 != " "})
        let isNewsHaveImage = url == " " ? false : true
        
        view = detailNewsView.setConstraints(view: view, isImageExist: isNewsHaveImage)
        
        let webBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "safari"),
                                            style: .done,
                                            target: self,
                                            action: #selector(showSiteVersion))
        
        navigationItem.rightBarButtonItem = webBarButtonItem
    }
    
    private func setDataToView() {
        DispatchQueue.main.async {
            self.detailNewsView.mainTitlelabel.text? = self.presenter.news.title.html2String.localizedCapitalized
            self.detailNewsView.fullNewsLabel.text = self.presenter.news.fullNews.html2String
        }
        presenter.downloadImage()
    }
    
    @objc func showSiteVersion() {
        presenter.showFullNews()
    }

}

extension DetailNewsVC: DetailNewsViewProtocol {
    func successDownloadImage(image: Data) {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.4) {
                self.detailNewsView.mainImageView.alpha = 1.0
            }
            self.detailNewsView.mainImageView.image = UIImage(data: image)
        }
    }
    
    func failureDownloadImage() {
      print("failureDownloadImageuppp")
    }
}

