//
//  DetailNewsPresenter.swift
//  Vesti.ru
//
//  Created by a on 24.09.2020.
//  Copyright © 2020 a. All rights reserved.
//

import Foundation

protocol DetailNewsViewProtocol: class {
    func successDownloadImage(image: Data)
    func failureDownloadImage()
}

protocol DetailNewsPresenterProtocol {
    init(news: RSSItem, view: DetailNewsViewProtocol, model: DetailNewsModelProtocol, coordinator: NewsCoordinatorProtocol)
    var news: RSSItem { get }
    func downloadImage()
    func showFullNews()
}

final class DetailNewsPresenter: DetailNewsPresenterProtocol {
    
    var news: RSSItem
    weak var view: DetailNewsViewProtocol?
    private var coordinator: NewsCoordinatorProtocol
    private var model: DetailNewsModelProtocol
    
    init(news: RSSItem, view: DetailNewsViewProtocol, model: DetailNewsModelProtocol, coordinator: NewsCoordinatorProtocol) {
        self.news = news
        self.view = view
        self.model = model
        self.coordinator = coordinator
    }
    
    func downloadImage() {
        model.downloadImage(urlAdress: news.imageURL) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .failure(_):
                self.view?.failureDownloadImage()
            case .success(let data):
                self.view?.successDownloadImage(image: data)
            }
        }
    }
    
    func showFullNews() {
        coordinator.presentWebView(url: news.link.html2String.filter({$0 != " "}))
    }
    
}

