//
//  NewsCoordinator.swift
//  Vesti.ru
//
//  Created by a on 24.09.2020.
//  Copyright © 2020 a. All rights reserved.
//

import UIKit
import SafariServices

protocol NewsCoordinatorProtocol: СoordinatorProtocol {
    func showDetail(news: RSSItem)
    func presentWebView(url: String)
}

final class NewsCoordinator: NewsCoordinatorProtocol {
    
    var assemblyBuilder: AssemblyBuilderProtocol?
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.assemblyBuilder = AssemblyBuilder()
        start()
    }
    
    func start() {
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.titleTextAttributes = [.foregroundColor: UIColor.titleColor!]
            appearance.largeTitleTextAttributes = [.foregroundColor: UIColor.titleColor!]
            
            UINavigationBar.appearance().tintColor = UIColor.titleColor
            UINavigationBar.appearance().standardAppearance = appearance
            UINavigationBar.appearance().compactAppearance = appearance
            UINavigationBar.appearance().scrollEdgeAppearance = appearance
        } else {
            UINavigationBar.appearance().tintColor = UIColor.titleColor
            UINavigationBar.appearance().isTranslucent = false
        }
        
        let newsVC = assemblyBuilder!.createMainNewsModule(coordinator: self)
        navigationController.title = "Новости"
        navigationController.pushViewController(newsVC, animated: false)
    }
    
    func showDetail(news: RSSItem) {
        let detailVC = assemblyBuilder!.createDetailNewsModule(news: news, coordinator: self)
        navigationController.pushViewController(detailVC, animated: true)
    }
    
    func presentWebView(url: String) {
        let Url = URL(string: url)!
        let safari = SFSafariViewController(url: Url)
        safari.preferredControlTintColor = UIColor.titleColor
        safari.dismissButtonStyle = .close
        navigationController.present(safari, animated: true)
    }
    
}

