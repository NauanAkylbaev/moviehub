//
//  MainNewsPresenter.swift
//  Vesti.ru
//
//  Created by a on 24.09.2020.
//  Copyright © 2020 a. All rights reserved.
//

import Foundation

protocol MainNewsViewProtocol: class {
    func successShowNews()
    func failureShowNews()
    func showSortedNews()
}

protocol MainNewsPresenterProtocol: class {
    init(view: MainNewsViewProtocol, model: NewsModelProtocol, coordinator: NewsCoordinatorProtocol)
    var newsArray: [RSSItem] { get }
    var allNewsCategory: [String] { get }
    func getNews()
    func sortNews(theme: String)
    func showDetailNews(news: RSSItem)
}

final class MainNewsPresenter: MainNewsPresenterProtocol {
    
    weak var view: MainNewsViewProtocol?
    private let model: NewsModelProtocol
    private let coordinator: NewsCoordinatorProtocol
    
    var newsArray = [RSSItem]()
    var initialNewsArray = [RSSItem]()
    var allNewsCategory = [String]()
    
    init(view: MainNewsViewProtocol, model: NewsModelProtocol, coordinator: NewsCoordinatorProtocol) {
        self.view = view
        self.model = model
        self.coordinator = coordinator
    }
    
    func getNews() {
        model.downloadNews(url: .vesti) { [weak self] (result) in
            guard let self = self else { return }
            switch result {
            case .success(let news):
                self.newsArray = news
                self.view?.successShowNews()
                self.initialNewsArray = self.newsArray
                self.allNewsCategory = ["Все"]
                self.allNewsCategory += Array(Set(self.newsArray.map({$0.category}))) // all unique category
            case .failure(let err):
                self.newsArray = [RSSItem(title: "Внимание",
                                                description: "Возникла ошибка, проверьте соединение с интернетом, либо зайдите позже",
                                                publicDate: "",
                                                link: "",
                                                category: "",
                                                fullNews: "",
                                                imageURL: "")]
                
                print(err.localizedDescription)
                self.view?.failureShowNews()
            }
        }
    }
    
    func showDetailNews(news: RSSItem) {
        coordinator.showDetail(news: news)
    }
    
    func sortNews(theme: String) {
        if theme == "Все" {
            newsArray = initialNewsArray
        } else {
            newsArray = initialNewsArray.filter({$0.category == theme})
        }
        
        view?.showSortedNews()
    }
    
}

