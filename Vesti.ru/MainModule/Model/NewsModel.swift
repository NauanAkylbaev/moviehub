//
//  NewsModel.swift
//  Vesti.ru
//
//  Created by a on 24.09.2020.
//  Copyright © 2020 a. All rights reserved.
//

import Foundation

protocol NewsModelProtocol: class {
    init(networkServices: NewsServicesProtocol)
    func downloadNews(url: URLAdresses, completionHandler: @escaping ((Result<[RSSItem], Error>) -> Void))
}


final class NewsModel: NewsModelProtocol {
    
    private let networkServices: NewsServicesProtocol
    
    init(networkServices: NewsServicesProtocol) {
        self.networkServices = networkServices
    }
    
    func downloadNews(url: URLAdresses, completionHandler: @escaping ((Result<[RSSItem], Error>) -> Void)) {
        networkServices.downloadNews(url: url.rawValue) { result in
            switch result {
            case .failure(let err):
                completionHandler(.failure(err))
            case .success(let data):
                completionHandler(.success(data))
            }
        }
    }
    
}

