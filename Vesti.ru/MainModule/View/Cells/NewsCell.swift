//
//  NewsCell.swift
//  Vesti.ru
//
//  Created by a on 24.09.2020.
//  Copyright © 2020 a. All rights reserved.
//

import UIKit

final class NewsCustomCell: UICollectionViewCell {
    
    public var item: RSSItem! {
        didSet {
            DispatchQueue.main.async {
                self.titleLabel.text = self.item.title.html2String
                self.descriptionLabel.text = self.item.description.html2String
                self.timeLabel.text = Date.returnStringDay(currentDate: self.item.publicDate.html2String)
            }
        }
    }
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Helvetica-Bold", size: 18)
        label.numberOfLines = 0
        label.textAlignment = .left
        return label
    }()
    
    private let descriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.textColor?.withAlphaComponent(0.6)
        label.font = UIFont(name: "Helvetica", size: 16)
        label.numberOfLines = 0
        label.textAlignment = .left
        return label
    }()
    
    private let timeLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Helvetica-Bold", size: 14)
        label.numberOfLines = 0
        label.textAlignment = .right
        return label
    }()
    
    override init(frame: CGRect) {
        super .init(frame: frame)
        customizeCell()
        setConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func customizeCell() {
        contentView.backgroundColor = .cellColor
    }
    
    private func setConstraints() {
        contentView.addSubview(titleLabel)
        contentView.addSubview(descriptionLabel)
        contentView.addSubview(timeLabel)
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: +15),
            titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: +15),
            titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15),
        ])
        
        NSLayoutConstraint.activate([
            descriptionLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: +10),
            descriptionLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: +15),
            descriptionLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15),
            descriptionLabel.bottomAnchor.constraint(equalTo: timeLabel.topAnchor, constant: -15)
        ])
        
        NSLayoutConstraint.activate([
            timeLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15),
            timeLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -15),
            timeLabel.heightAnchor.constraint(equalToConstant: 10)
        ])
        
    }
    
}

