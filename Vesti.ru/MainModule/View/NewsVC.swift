//
//  NewsVC.swift
//  Vesti.ru
//
//  Created by a on 24.09.2020.
//  Copyright © 2020 a. All rights reserved.
//

import UIKit

final class NewsCVC: UICollectionViewController {
    
    public var presenter: MainNewsPresenterProtocol!
    private let newsCVCView = NewsCVCView()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "Новости"
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        customizeView()
        downloadNews()
    }
    
    private func customizeView() {
        title = "Новости"
        view.backgroundColor = .backGroundColor
        collectionView.backgroundColor = .backGroundColor
        collectionView.register(NewsCustomCell.self, forCellWithReuseIdentifier: "Cell")
        newsCVCView.startIndicator(view: view)
    }
    
    private func setupNavBar() {
        let categoryBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "slider.horizontal.3"),
                                                    style: .done,
                                                    target: self,
                                                    action: #selector(showAlertSheet))
        
        navigationItem.rightBarButtonItem = categoryBarButtonItem
        
        collectionView.refreshControl = newsCVCView.refreshControl
        newsCVCView.refreshControl.addTarget(self, action: #selector(downloadNews), for: .valueChanged)
    }
    
    @objc private func showAlertSheet() {
        let actionSheetController = UIAlertController(title: "Выберите категорию", message: nil, preferredStyle: .actionSheet)
        
        for category in presenter.allNewsCategory {
            let action = UIAlertAction(title: category, style: .default) { [weak self] action -> Void in
                self?.presenter.sortNews(theme: category)
                UIView.animate(withDuration: 1.0) {
                    self?.collectionView.alpha = 0.0
                }
            }
            actionSheetController.addAction(action)
        }
        
        present(actionSheetController, animated: true, completion: nil)
    }
    
    @objc private func downloadNews() {
        UIView.animate(withDuration: 1.0) {
            self.collectionView.alpha = 0.0
        }
        presenter.getNews()
    }
    
}

extension NewsCVC: MainNewsViewProtocol {
    func successShowNews() {
        DispatchQueue.main.async {
            self.collectionView.reloadData()
            self.newsCVCView.stopIndicator()
            self.newsCVCView.refreshControl.endRefreshing()
            UIView.animate(withDuration: 1.0) {
                self.collectionView.alpha = 1.0
            }
        }
    }
    
    func failureShowNews() {
        DispatchQueue.main.async {
            self.collectionView.reloadData()
            self.newsCVCView.stopIndicator()
            self.newsCVCView.refreshControl.endRefreshing()
            UIView.animate(withDuration: 1.0) {
                self.collectionView.alpha = 1.0
            }
        }
    }
    
    func showSortedNews() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 1.0) {
                self.collectionView.alpha = 1.0
            }
            self.collectionView.reloadData()
            self.collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .top, animated: true)
        }
    }
}

extension NewsCVC: UICollectionViewDelegateFlowLayout {
    
    // MARK: - UICollectionViewDataSource
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.newsArray.count
    }
    
    // MARK: - UICollectionViewDelegate
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! NewsCustomCell
        cell.item = presenter.newsArray[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let currentText = presenter.newsArray[indexPath.item].title
        
        let size = CGSize(width: view.bounds.width - 30, height: 1000)
        let attribute = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 18)]
        let knownHeight: CGFloat = 100 + 15 + 10 + 5 + 10 + 15 // 100 description news
        
        let rect = NSString(string: currentText).boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: attribute, context: nil)
        
        return CGSize(width: view.bounds.width, height: rect.height + knownHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let news = presenter.newsArray[indexPath.item]
        presenter.showDetailNews(news: news)
    }
}

