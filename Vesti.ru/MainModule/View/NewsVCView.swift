//
//  NewsVCView.swift
//  Vesti.ru
//
//  Created by a on 24.09.2020.
//  Copyright © 2020 a. All rights reserved.
//

import UIKit

final class NewsCVCView {
    
    public let activityIndicator = UIActivityIndicatorView()
    public let refreshControl = UIRefreshControl()
    
    public func startIndicator (view: UIView) {
        activityIndicator.center = view.center
        activityIndicator.style = .large
        activityIndicator.color = .textColor
        activityIndicator.startAnimating()
        view.addSubview(activityIndicator)
    }
    
    public func stopIndicator() {
        self.activityIndicator.stopAnimating()
        self.activityIndicator.removeFromSuperview()
    }

}
